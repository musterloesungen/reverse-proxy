# Beispiel für die Docker-Compose-Datei

```yaml
networks:
  internal_network:

volumes:
  andreas_volume:

services:
  nginx:
    image: nginx:latest
    container_name: nginx_service
    networks:
      internal_network:
        aliases:
          - andreas
    volumes:
      - andreas_volume:/usr/share/nginx/html
    restart: always

  perl_writer:
    image: perl:latest
    container_name: perl_writer
    command: perl -e "open(my $fh, '>', '/data/index.html') or die 'Cannot open file: $!'; print $fh 'Hallo Welt\n'; close $fh;"

    volumes:
      - andreas_volume:/data
    network_mode: none
    restart: "no"
```
