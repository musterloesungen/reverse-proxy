# Aufgabe: Individuelles Setup mit Nginx und Perl

## Ziel
Setze ein Docker-Setup um, bei dem ein `nginx`-Container im internen Netzwerk betrieben wird, der eine statische Website ausliefert. Der Inhalt der Website stammt aus einer Datei (`index.html`), die in einem Volume gespeichert ist. Ein separater `perl`-Container erzeugt oder aktualisiert diese Datei.

## Anforderungen
1. **Nginx-Container**:
   - Läuft im `internal_network`.
   - Nutzt ein eigenes Volume, das auf `/usr/share/nginx/html` gemountet ist.
   - Liefert die Datei `index.html` aus dem Volume als statische Website aus.

2. **Perl-Container**:
   - Erstellt oder aktualisiert die Datei `index.html` im selben Volume.
   - Der Inhalt der Datei kann frei gestaltet werden; ein einfacher Text wie `Hallo Welt` ist ausreichend.

3. **Netzwerkkonfiguration**:
   - Der `nginx`-Container ist im internen Netzwerk unter einem individuellen Netzwerk-Alias erreichbar. Dieser Alias sollte deinem Vornamen entsprechen.

---

## Schritte zur Umsetzung

### 1. Erstelle das Setup

1. Definiere ein Netzwerk (`internal_network`), um die Kommunikation zwischen den Containern zu ermöglichen.
2. Erstelle ein Volume, das von beiden Containern gemeinsam genutzt wird.
3. Konfiguriere den `nginx`-Container:
   - Binde das Volume auf den Pfad `/usr/share/nginx/html`.
   - Füge den Netzwerk-Alias hinzu.
4. Konfiguriere den `perl`-Container:
   - Nutze das gleiche Volume.
   - Schreibe ein einfaches Kommando, das die Datei `index.html` mit Inhalt erstellt oder aktualisiert.

### 2. Starte die Container
Führe das Docker-Compose-Setup aus und überprüfe, ob die `index.html` erfolgreich erstellt wurde.

### 3. Teste die Webseite
1. Greife mit `curl` oder einem Browser auf den `nginx`-Container zu, indem du den Netzwerk-Alias verwendest.
2. Stelle sicher, dass die Datei `index.html` korrekt ausgeliefert wird.

### 4. Erweiterung (optional)
Passe den Inhalt der `index.html` an, starte den `perl`-Container erneut und überprüfe, ob die Änderungen von `nginx` übernommen werden.
