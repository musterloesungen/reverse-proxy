# README.md für Portainer mit Nginx Reverse Proxy

## Einleitung
Diese Anleitung beschreibt die Einrichtung von Portainer, einem Container-Management-Tool, das über einen Nginx Reverse Proxy erreichbar ist. Der Zugriff auf Portainer erfolgt entweder über den Endpunkt `/portainer` auf Port 80 oder direkt auf Port 9443.

## Voraussetzungen
- Docker und Docker Compose installiert
- Zugriff auf das Terminal oder die Kommandozeile

## Schritte zur Einrichtung

### 1. Erstellen der Docker Compose-Datei
Erstellen Sie eine Datei mit dem Namen `docker-compose.yml` und fügen Sie den folgenden Inhalt hinin:

```yaml
version: '3.8'

services:
  portainer:
    image: portainer/portainer-ce:latest
    container_name: portainer
    restart: always
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - portainer_data:/data

  nginx:
    image: nginx:latest
    restart: always
    volumes:
      - ./nginx.conf:/etc/nginx/nginx.conf
    ports:
      - "80:80"
      - "9443:9443"
    depends_on:
      - portainer

volumes:
  portainer_data:
```

### 2. Erstellen der Nginx-Konfigurationsdatei
Erstellen Sie eine Datei namens `nginx.conf` im gleichen Verzeichnis wie Ihre Docker Compose-Datei. Fügen Sie den folgenden Inhalt hinzu:

```nginx
events {}

http {
    server {
        listen 80;

        location /portainer/ {
            proxy_pass http://portainer:9000/;
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header X-Forwarded-Proto $scheme;
        }

        location / {
            return 301 /portainer/;
        }
    }

    server {
        listen 9443;

        location / {
            proxy_pass http://portainer:9000/;
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header X-Forwarded-Proto $scheme;
        }
    }
}
```

### 3. Starten der Services
Führen Sie im Verzeichnis, das Ihre `docker-compose.yml` und `nginx.conf` enthält, den folgenden Befehl aus:

```bash
docker compose up -d
```

### 4. Zugriff auf Portainer
Nach dem Starten der Services können Sie Portainer auf folgende Weisen erreichen:
- Über einen Webbrowser bei `http://IhreServerIP/portainer/`

## Hinweise
- Stellen Sie sicher, dass die Ports 80 und 9443 auf Ihrem Server offen sind.
